import java.io.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SmtpServerMain {
	public static void main(String[] args) throws Exception {
		ExecutorService executor = null;
		ServerSocket serverSocket = new ServerSocket(3000);
		Socket client = null;
		int limit = 5;
		try {
			executor = Executors.newFixedThreadPool(10);
			System.out.println("  > Port "+3000);
			System.out.println("  > Ready");
			while(true){
				System.out.println("  > Connected Clients: "+SmtpServer.total);
				client = serverSocket.accept();
				if(SmtpServer.total<limit){
					Runnable worker = new SmtpServer(client);
					executor.execute(worker);
				} else {
					client.close();
				}
			}
		} catch (Exception e) {
			executor.shutdown();
			System.out.println(e);
			System.out.println("Finished all threads");
		}
	}

}

// class WorkerThread implements Runnable {

// 	private String command;
// 	private Socket client;
// 	public static int id = 0;

// 	public WorkerThread(String s, Socket client){
// 		this.command = s;
// 		this.client = client;
// 		WorkerThread.id++;
// 	}

// 	@Override
// 	public void run() {
// 		System.out.println(Thread.currentThread().getName()+" Start. Command = "+command);
// 		processCommand();
// 		System.out.println(Thread.currentThread().getName()+" End.");
// 	}

// 	private void processCommand() {
// 		try {
// 			PrintWriter out = new PrintWriter(client.getOutputStream(), true);
// 			out.println("HTTP/1.1 200 OK");
// 			out.println("Date: Mon, 27 Jul 2015 21:36:42 GMT");
// 			out.println("Expires: -1");
// 			out.println("Server: queti/1.0.0");
// 			out.println("Access-Control-Allow-Origin: *");
// 			out.println("Content-Type: application/json; charset=UTF-8\n");
// 			out.print("{id: \""+command+"\"}");
// 			out.flush();
// 			Thread.sleep(5000);
// 			out.close();
// 			this.client.close();
// 			WorkerThread.id--;
// 		} catch (InterruptedException e) {
// 			e.printStackTrace();
// 		} catch(Exception e){
// 			e.printStackTrace();
// 		}
// 	}

// 	@Override
// 	public String toString(){
// 		return this.command;
// 	}
// }

class SmtpServer implements Runnable{
	private String from, server, to, content, domain;
	private int step;
	private int OK=250, SYNTAX=555, UNKNOWN=502, NOTFOUND=404;
	private int HELO=0, MAILFROM=1, RCPT=2, DATA=3, DATA2=4, DONE=5;
	private UUID id;
	private ServerSocket serverSocket;
	private PrintWriter out;
	private Socket client;
	public static int total = 0;
	private DBDriver database;
	private boolean local;

	/*
	 * - - - constructores - - - -
	 */
	public SmtpServer(String server, Socket client){
		SmtpServer.total++;
		this.from = "";
		this.to = "";
		this.content = "";
		this.server = server;
		this.step = 0;
		this.id = UUID.randomUUID();
		this.client = client;
		this.local = false;
		this.database = new DBDriver();
		// try {
			// client.setSoTimeout(10000);
		// } catch (Exception e) {
			
		// }
	}
	public SmtpServer(Socket client){
		this("mx.server.demo", client);
	}

	
	/*
	 * - - - Metodos - - - -
	 */
	public void helo(String line){
		nextStep(MAILFROM);
		out.println("250 "+this.server+" at your service");
	}
	public void mailFrom(String line){
		if(this.step<MAILFROM) { this.exit(503); return; }

		if( SmtpServer.contains(line, "<") &&
			SmtpServer.contains(line, ">") && 
			SmtpServer.contains(line, ":") && 
			SmtpServer.contains(line, "@")) {

			this.from = line.substring(line.indexOf(": ")+2);
			String domain = "";
			
			try {
			 	domain = from.substring(from.indexOf("@")+1, from.indexOf(">"));
			} catch (Exception e) {
				System.out.println("Error");
				System.out.println(e);
			}

			ArrayList<String> info = database.DNSFindOne(domain);
			
			if(info.get(0).equals("NOT-FOUND")){
				exit(NOTFOUND);
			} else {
				nextStep(RCPT);
				this.exit(OK);
			}
			
		} else {
			this.step = 1;
			this.exit(SYNTAX);
		}
	}
	public void rcpt(String line){
		if(this.step<RCPT) { this.exit(503); return; }
		if( SmtpServer.contains(line, "to") &&
			SmtpServer.contains(line, "<") &&
			SmtpServer.contains(line, ">") && 
			SmtpServer.contains(line, ":") && 
			SmtpServer.contains(line, "@")) {

			this.to = line.substring(line.indexOf(": ")+2);
			String domain = "";
			
			try {
			 	domain = to.substring(to.indexOf("@")+1, to.indexOf(">"));
			} catch (Exception e) {
				System.out.println("Error");
				System.out.println(e);
			}

			ArrayList<String> info = database.DNSFindOne(domain);

			
			if(info.get(0).equals("NOT-FOUND")){
				exit(NOTFOUND);
			} else {
				if(info.get(0).equals("127.0.0.1"))
					local = true;
				nextStep(DATA);
				this.domain = domain;
				if(local)
					exit(OK);
				else
					exit(251);
			}
		} else
			exit(SYNTAX);
	}
	public void data(String line){
		if(this.step<DATA) { this.exit(503); return; }
		if(line.equals("."))
			nextStep(DONE);
		else
			this.content += line+"\n";
	}
	public void checkSequence(){
		if(this.step<DATA) { this.exit(503); return; }
		out.println("354 GO AHEAD");
		nextStep(DATA2);
	}


	/*
	 * - - - Extras - - - -
	 */
	public void nextStep(int next){
		if((this.step==next-1))
			println("next: "+next);
		this.step = (this.step==next-1)? next: step;
	}
	public static boolean contains(String line, String value){
		return (line.toLowerCase().indexOf(value)!=-1);
	}
	public void exit(int code){
		String phase = "";

		switch (code) {
			case 555:
				out.println("501 Syntax error.");
				break;
			case 503:
				phase = (this.step<MAILFROM)? "EHLO/HELO": "MAIL";
				phase = (this.step==RCPT)? "RCPT": phase;
				out.println("503 Error: "+phase+" first.");
				break;
			case 404:
				phase = (this.step==MAILFROM)? "reciever":"sender";
				out.println("553 Domain of "+phase+" address does not exist");
				break;
			case 502:
				out.println("502 Unrecognized command.");
				break;
			case 251:
				out.println("251 Forwarding email");
				break;
			case 250:
				out.println("250 OK");
				break;
		}
	}
	public void print(Object s){
		System.out.print(s);
	}

	public void println(Object s){
		System.out.println(s);
	}

	/*
	 *	- - - SEND MAIL - - - 
	 */
	public void sendEmail(){
		
	}
	public void saveEmail(){
		if(!content.equals("")){
			content = content.substring(0, content.length()-1);
		}

		if(this.local){
			database.inbox(this.from, this.to, this.content, this.id.toString().substring(0,8));
		} else {
			database.outgoingMail(this.from, this.to, this.content, this.id.toString().substring(0,8), this.domain);
		}
	}


	/*
	 * - - - MAIN - - - -
	 */
	public void run(){
		System.out.println(Thread.currentThread().getName()+" Start.");
		start();
		System.out.println(Thread.currentThread().getName()+" End.");
		try {
			this.client.close();	
		} catch (Exception e) {
			System.out.println(e);
		}
		SmtpServer.total--;
	}
	public void start(){
		// campos
		// Scanner sc = new Scanner(System.in);
		String line = "";
		int pos = -1;
		BufferedReader in = null;

		// TCP
		// Socket clientSocket = null; 
		// System.out.println ("\tWaiting for connection.....");

		try { 
			// clientSocket = serverSocket.accept();
			this.out = new PrintWriter(this.client.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader( this.client.getInputStream()));
		} catch (IOException e)  {
			System.err.println(e);
			return;
			// System.exit(1);
		}

		try {
			while(this.step!=DONE && !(line = in.readLine()).equals("quit")){
				if(SmtpServer.contains(line, "data")) checkSequence();
				else if(this.step==DATA2) data(line);
				else if(SmtpServer.contains(line, "helo")) helo(line);
				else if(SmtpServer.contains(line, "mail from")) mailFrom(line);
				else if(SmtpServer.contains(line, "rcpt")) rcpt(line);
				else this.exit(UNKNOWN);
				Thread.sleep(1000);
			}

			out.println(this);
			println("done");
		} catch (Exception e) {
			// e.printStackTrace(System.out);
			System.err.println("  > Client Disconnected\n  > Read Fail");
		}

		try {
			out.close(); 
			in.close(); 
			this.client.close(); 
		} catch (Exception e) {
			println("fail");
			println(e);
			return;
			// System.exit(1);
		}
	}

	public String toString(){
		String message = "";
		if(from.equals("") || to.equals("")){
			message += "Email not sent!, missing params.";
		} else {
			saveEmail();
			message = "250 OK: queued as [ "+this.id.toString().substring(0, 8)+" ]";
			// message += "Email sent!\n";
			// message += "FROM: "+this.from+"\n";
			// message += "TO: "+this.to+"\n";
			// message += this.content + '\n';
			// message += "EMAIL ID: "+this.id+"\n";
			message += "\n221 "+this.server+" Service closing transmission channel\n\r";
		}
		return message;
	}
}
