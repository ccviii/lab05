make: run

run: server start

server:
	clear
	javac SmtpServerMain.java

start:
	java SmtpServerMain

clean:
	rm *.class

db:
	javac DBDriver.java

sender:
	javac Sender.java
	java Sender


