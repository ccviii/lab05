import java.io.*;
import java.util.*;
import java.net.*;

public class Sender {
	private Socket tcpSocket;
	private PrintWriter out;
	private BufferedReader in;
	
	// constructor
	public Sender(String host, int port){
		Socket tcpSocket = null;
		PrintWriter out = null;
		BufferedReader in = null;
		try {
			tcpSocket = new Socket(host, port);
			out = new PrintWriter(tcpSocket.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(tcpSocket.getInputStream()));
		} catch (UnknownHostException e) {
			System.err.println("Don't know about host: " + host);
			//System.err.println("");
			//System.exit(0);
		} catch (IOException e) {
			System.err.println("Couldn't get I/O for "+ "the connection to: " + host);
			//System.err.println("");
			//System.exit(0);
		}
		this.tcpSocket = tcpSocket;
		this.out = out;
		this.in = in;
	}
	public Sender(){
		this("localhost", 1337);
	}
	public Sender(String host){
		this(host, 1337);
	}

	public void print(Object s){
		System.out.print(s);
	}

	public void println(Object s){
		System.out.println(s);
	}

	public int sendMail(ArrayList<String> mail) {
		// BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
		// String userInput;

		// print("$ ");

		try {
		
			this.out.println("helo mx.jorge.gt");
			println(this.in.readLine());
			this.out.println("mail from: "+mail.get(2));
			println(this.in.readLine());
			this.out.println("rcpt to: "+mail.get(1));
			println(this.in.readLine());
			this.out.println("data");
			println(this.in.readLine());
			this.out.println(mail.get(3));
			this.out.println(".");
			println(this.in.readLine());

			this.out.close();
			this.in.close();
			this.tcpSocket.close();

		} catch (Exception e) {
			print(e);
		}

		return 0;
	}

	public static void main(String[] args) {
		DBDriver database = new DBDriver();
		ArrayList<ArrayList<String>> mails = new ArrayList<ArrayList<String>>();
		ArrayList<String> mail = null;
		Sender conexion = null;

		while(true){
			try {
				System.out.println("  > check");
				mails = database.getOutgoingMail();
				for (int i=0; i<mails.size(); i++) {
					mail = mails.get(i);
					if(mail.get(4).equals("false")){
						ArrayList<String> info = database.DNSFindOne(mail.get(5));
						System.out.println("Sending to "+info.get(0));
						conexion = new Sender(info.get(0), Integer.parseInt(info.get(1)));
						int status = conexion.sendMail(mail);
						database.updateMail(mail);
						System.out.println("  > done");
					}
				}
			    Thread.sleep(1000);
			} catch (Exception e) {
			    //Handle exception
			    // System.out.println("Error--");
			    e.printStackTrace(System.out);
			    System.exit(1);
			}
		}
	}
}
