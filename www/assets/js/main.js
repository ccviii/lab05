'use strict';

angular.module("app", [])
.controller('mainCtrl', function($scope, $http, $timeout){
	$scope.outgoing = [{from: "loading"}];
	$scope.inbox = [{from: "loading"}];

	$scope.tabs = {
		default: 1,
		set: function(num){
			$scope.tabs.default = num;
		},
		get: function() {
			return $scope.tabs.default;
		},
		inbox: 1,
		outbox: 2,
		create: 3
	};
	$scope.email = {
		from: "<user@jorge.gt>",
		to: "",
		body: "",
		emailId:"",
		sent: false,
		domain: "",
		date: ""
	}

	var outBox = function(){
		$http.get('/outgoing').success(function(mails){
			$scope.outgoing = mails;
		})
	};

	var inBox = function(){
		$http.get('/inbox').success(function(mails){
			$scope.inbox = mails;
		})
	};

	$scope.sendMail = function() {
		$scope.email.emailId = "WEB-"+Math.floor(Math.random()*10000);
		$scope.email.date = "2015/08/25 10:08:10";
		$scope.email.domain = $scope.email.to.substring($scope.email.to.indexOf("@")+1, 
			$scope.email.to.indexOf(">"));

  		$http.post('/outgoing', $scope.email)
	  	$timeout(function(){
	  		$scope.email = {
				from: "<user@jorge.gt>",
				to: "",
				body: "",
				emailId:"",
				sent: false,
				domain: "",
				date: ""
			}
	  	}, 2000)
	}

	outBox();
	inBox();

	setInterval(outBox, 5000);
	setInterval(inBox, 5000);
})
