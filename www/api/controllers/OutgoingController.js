/**
 * OutgoingController
 *
 * @description :: Server-side logic for managing Outgoings
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	find: function(req, res){
		Outgoing.find().sort({date: -1}).then(function(mails) {
			res.json(mails);
		})
	}
};

