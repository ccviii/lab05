/**
 * InboxController
 *
 * @description :: Server-side logic for managing inboxes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	find: function(req, res){
		Inbox.find().sort({date: -1}).then(function(mails){
			res.json(mails);
		});
	}
};

