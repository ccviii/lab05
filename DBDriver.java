import com.mongodb.BasicDBObject;
import com.mongodb.BulkWriteOperation;
import com.mongodb.BulkWriteResult;
import com.mongodb.Cursor;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.ParallelScanOptions;
import com.mongodb.ServerAddress;
import com.mongodb.MongoCredential;

import java.util.List;
import java.util.Set;
import java.util.ArrayList;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static java.util.concurrent.TimeUnit.SECONDS;

public class DBDriver {
	private DB db;
	private static int ID=0, TO=1, FROM=2, BODY=3, SENT=4;

	public DBDriver(String database, String host, int port, boolean auth) {
		try {
			MongoClient mongoClient = null;
		//	if(auth){
		//		MongoCredential credential = MongoCredential.createCredential("normal", database, "Ab1234");
		//		mongoClient = new MongoClient(new ServerAddress(), Arrays.asList(credential));
		//	} else {
				mongoClient = new MongoClient(host, port);
		//	}
			this.db = mongoClient.getDB(database);
		} catch (Exception e) {
			System.out.println(e);
			System.exit(1);
		}
	}

	public DBDriver(){
		// mongodb://<dbuser>:<dbpassword>@ds035723.mongolab.com:35723/smtp-email
		this("smtp-email", "localhost", 27017, false);
	}

	public void close(){
		// MongoClient.close();
	}

	public void DNSInsert(String domain, String ip, String port){
		DBCollection coll = this.db.getCollection("dns");

		BasicDBObject doc = new BasicDBObject("domain", domain)
		.append("ip", ip)
		.append("port", port);

		coll.insert(doc);
	}

	public ArrayList<String> DNSFindOne(String domain){
		DBCollection coll = this.db.getCollection("dns");
		DBCursor cursor = coll.find();
		ArrayList<String> client = new ArrayList<String>();
		client.add("NOT-FOUND");

		try {
			while(cursor.hasNext()) {
				DBObject domainDB = cursor.next();
				
				Object ip 		= domainDB.get("ip");
				Object port 	= domainDB.get("port");
				Object name 	= domainDB.get("domain");

				ArrayList<String> obj = new ArrayList<String>();
				obj.add(ip.toString());
				obj.add(port.toString());
				obj.add(name.toString());

				// System.out.println(name+"");

				if(obj.get(2).equals(domain)){
					client=obj;
				}
			}
		} finally {
			// System.out.println("ninguno");
			cursor.close();
		}

		return client;
	}

	public void outgoingMail(String from, String to, String body, String id, String domain) {
		DBCollection coll = this.db.getCollection("outgoing");

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		String fecha = dateFormat.format(cal.getTime()); // 2014/08/06 16:00:22

		BasicDBObject doc = new BasicDBObject("from", from)
		.append("to", to)
		.append("body", body)
		.append("emailId", id)
		.append("sent", false)
		.append("domain", domain)
		.append("date", fecha);

		coll.insert(doc);
	}

	public void updateMail(ArrayList<String> mail){
		DBCollection coll = this.db.getCollection("outgoing");

		BasicDBObject searchQuery = new BasicDBObject("emailId", mail.get(0));
		
		BasicDBObject newDocument = new BasicDBObject("from", mail.get(2))
		.append("to", mail.get(1))
		.append("body", mail.get(3))
		.append("emailId", mail.get(0))
		.append("sent", true)
		.append("domain", mail.get(5))
		.append("date", mail.get(6));

		coll.update(searchQuery, newDocument);
	}

	public void inbox(String from, String to, String body, String id) {
		DBCollection coll = this.db.getCollection("inbox");

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		String fecha = dateFormat.format(cal.getTime()); // 2014/08/06 16:00:22

		BasicDBObject doc = new BasicDBObject("from", from)
		.append("to", to)
		.append("body", body)
		.append("emailId", id)
		.append("date", fecha);

		coll.insert(doc);
	}

	public ArrayList<ArrayList<String>> getOutgoingMail(){
		DBCollection coll = this.db.getCollection("outgoing");
		DBCursor cursor = coll.find();
		ArrayList<ArrayList<String>> emails = new ArrayList<ArrayList<String>>();
		try {
			// System.out.println("    id     |   sent");
			// System.out.println("-----------------------");
			while(cursor.hasNext()) {
				DBObject email = cursor.next();
				
				Object sent 	= email.get("sent");
				Object id 		= email.get("emailId");
				Object to 		= email.get("to");
				Object from 	= email.get("from");
				Object content 	= email.get("body");
				Object domain 	= email.get("domain");
				Object date 	= email.get("date");

				ArrayList<String> mail = new ArrayList<String>();
				mail.add(id+"");
				mail.add(to+"");
				mail.add(from+"");
				mail.add(content+"");
				mail.add(sent+"");
				mail.add(domain+"");
				mail.add(date+"");

				emails.add(mail);

				// System.out.println("  "+id+" |  "+sent);
				// System.out.print(email);

			}
		} finally {
			
			cursor.close();
		}
		return emails;
	}
	public void printOutgoing(){
		ArrayList<ArrayList<String>> emails = this.getOutgoingMail();
		for (int i=0; i<emails.size(); i++) {
			System.out.println(emails.get(i).get(ID));
		}
	}

	// public static void main(String[] args) {
	// 	DBDriver con = new DBDriver();

	// 	// while(true){
	// 		try {
	// 			con.printOutgoing();
	// 		    Thread.sleep(5000);
	// 		} catch (Exception e) {
	// 		    //Handle exception
	// 		    e.printStackTrace(System.out);
	// 		}
	// 	// }
	// }
}
